package ru.ekfedorov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.ekfedorov.tm.cofiguration.ClientConfiguration;
import ru.ekfedorov.tm.marker.IntegrationCategory;

import java.util.List;

public class ProjectEndpointTest {

    @NotNull
    public final AnnotationConfigApplicationContext context =
            new AnnotationConfigApplicationContext(ClientConfiguration.class);

    @NotNull
    private final SessionEndpoint sessionEndpoint = context.getBean(SessionEndpoint.class);

    @NotNull
    private final ProjectEndpoint projectEndpoint = context.getBean(ProjectEndpoint.class);

    private Session session;

    private Session sessionAdmin;

    @Before
    @SneakyThrows
    public void before() {
        session = sessionEndpoint.openSession("test", "test");
        sessionAdmin = sessionEndpoint.openSession("admin", "admin");
        projectEndpoint.clearBySessionProject(session);
        projectEndpoint.clearBySessionProject(sessionAdmin);
    }

    @After
    @SneakyThrows
    public void after() {
        projectEndpoint.clearBySessionProject(session);
        projectEndpoint.clearBySessionProject(sessionAdmin);
        sessionEndpoint.closeSession(session);
        sessionEndpoint.closeSession(sessionAdmin);
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void addProjectTest() {
        final String projectName = "nameTest";
        final String projectDescription = "nameTest";
        projectEndpoint.addProject(session, projectName, projectDescription);
        final Project project = projectEndpoint.findProjectOneByName(session, projectName);
        Assert.assertNotNull(project);
        Assert.assertEquals(projectName, project.getName());
        Assert.assertEquals(projectDescription, project.getDescription());
    }


    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByIdTest() {
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.changeProjectStatusById(session, project.getId(), Status.COMPLETE);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByIndexTest() {
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        final List<Project> projects = projectEndpoint.findProjectAll(session);

        int pos = 0;
        for(Project t : projects) {
            if(project.getId().equals(t.getId())) break;
            pos++;
        }

        projectEndpoint.changeProjectStatusByIndex(session, pos, Status.COMPLETE);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void changeProjectStatusByNameTest() {
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.changeProjectStatusByName(session, project.getName(), Status.COMPLETE);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findAllProjectTest() {
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest2", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        Assert.assertEquals(3, projectEndpoint.findProjectAll(session).size());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectAllWithComparatorTest() {
        projectEndpoint.addProject(session, "cprojectTest", "descrTest");
        projectEndpoint.addProject(session, "aprojectTest2", "descrTest");
        projectEndpoint.addProject(session, "bprojectTest3", "descrTest");
        final List<Project> projects = projectEndpoint.findProjectAllWithComparator(session, "NAME");
        Assert.assertEquals("aprojectTest2", projects.get(0).getName());
        Assert.assertEquals("bprojectTest3", projects.get(1).getName());
        Assert.assertEquals("cprojectTest", projects.get(2).getName());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByIdTest() {
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByIndexTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project project = projectEndpoint.findProjectAll(session).get(1);
        final Project projectFind = projectEndpoint.findProjectOneByIndex(session, 1);
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void findProjectOneByNameTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project projectFind = projectEndpoint.findProjectOneByName(session, "projectTest");
        Assert.assertNotNull(projectFind);
        Assert.assertEquals(project.getId(), projectFind.getId());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishProjectByIdTest() {
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.finishProjectById(session, project.getId());
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishProjectByIndexTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project project = projectEndpoint.findProjectAll(session).get(1);
        projectEndpoint.finishProjectByIndex(session, 1);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void finishProjectByNameTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        projectEndpoint.finishProjectByName(session, "projectTest");
        final Project projectChanged = projectEndpoint.findProjectOneByName(session, project.getName());
        Assert.assertEquals(Status.COMPLETE, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
        projectEndpoint.removeProject(session, project);
        Assert.assertNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByIdTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
        projectEndpoint.removeProjectOneById(session, project.getId());
        Assert.assertNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByIndexTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project project = projectEndpoint.findProjectAll(session).get(1);
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
        projectEndpoint.removeProjectOneByIndex(session, 1);
        Assert.assertNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void removeProjectOneByNameTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        Assert.assertNotNull(projectEndpoint.findProjectOneById(session, project.getId()));
        projectEndpoint.removeProjectOneByName(session, "projectTest");
        Assert.assertNull(projectEndpoint.findProjectOneById(session, project.getId()));
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startProjectByIdTest() {
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.startProjectById(session, project.getId());
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.IN_PROGRESS, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startProjectByIndexTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        final Project project = projectEndpoint.findProjectAll(session).get(1);
        projectEndpoint.startProjectByIndex(session, 1);
        final Project projectChanged = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(Status.IN_PROGRESS, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void startProjectByNameTest() {
        projectEndpoint.addProject(session, "projectTest0", "descrTest");
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.addProject(session, "projectTest3", "descrTest");
        projectEndpoint.startProjectByName(session, "projectTest");
        final Project projectChanged = projectEndpoint.findProjectOneByName(session, project.getName());
        Assert.assertEquals(Status.IN_PROGRESS, projectChanged.getStatus());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateProjectByIdTest() {
        final String newName = "projectTestNew";
        final String newDescription = "descrTestNew";
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.updateProjectById(session, project.getId(), newName, newDescription);
        final Project projectUpdate = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(newName, projectUpdate.getName());
        Assert.assertEquals(newDescription, projectUpdate.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(IntegrationCategory.class)
    public void updateProjectByIndexTest() {
        final String newName = "projectTestNew";
        final String newDescription = "descrTestNew";
        final Project project = projectEndpoint.addProject(session, "projectTest", "descrTest");
        projectEndpoint.updateProjectByIndex(session, 0, newName, newDescription);
        final Project projectUpdate = projectEndpoint.findProjectOneById(session, project.getId());
        Assert.assertEquals(newName, projectUpdate.getName());
        Assert.assertEquals(newDescription, projectUpdate.getDescription());
    }

}

